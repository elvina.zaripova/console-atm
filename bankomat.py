import time
# Function prints the instruction of console-atm
def get_instructions():
    """
    This function helps to display instructions for users
    
    """
    print (
    """ Добро пожаловать!
 Чтобы совершить какие-либо действия следуйте иструкции на экране.
    """
    )
# Function prints a receipt
def print_check(summa):
    """
    This function helps issue or not issue a receipt at an ATM
    
    :param summa: money on the card
    """
    print('Напечатать чек? (да\нет)') 
    answer = input()
    if answer == 'нет':
        print('Операция завершена')
    elif answer == 'да':
        print('Вы можете сберечь деревья. Уверены, что хотите напечатать чек?')
        answer = input()            
        if answer == 'да':
            print('Возьмите чек. Баланс:', summa)
            print('Операция завершена')
        elif answer == 'нет':
            print('Спасибо.Операция завершена')
    else:
        print('Ошибка. Вставьте карту еще раз и повторите действие')
while True:
    get_instructions()
    surname = input('Введите фамилию(ENG):') 
    for i in range(len(surname)):
        if  'A' <= surname[i] <= 'Z' or 'a' <= surname[i] <= 'z':
            flag = True
        elif ord('0') <= ord(surname[i]) <= ord('9'):
            flag = False
            break
    if not flag :
        print('Ошибка, введите ФИО занаво')
        time.sleep(1)
    else:
        f = open('text.txt','w')
        f.write(surname + '-') 
        f.close() 
        break 
# Print how much money     
summa =  int(input('Введите имеющуюся сумму:'))  
f = open('text.txt','a')
f.write(str(summa)) 
f.close() 
# Take certain actions
print('Вставьте карту')
# if you chose to take off money
print('Введите действие (снять\положить): ')
p = input()
if p == 'снять':    
    getmoney = int(input('Введите необходимую сумму для снятия: '))
    if (getmoney > 0) and (getmoney % 50 == 0):
        while getmoney >= 50:
            if (getmoney % 50 == 0) and (getmoney <= summa):
                print('Заберите деньги')
                summa = summa - getmoney 
                break
            elif (getmoney > summa):
                print('Недостаточно средств')
                break
            else:
                print('Невозможно снять данную сумму. Банкомат выдает купюры: 5000,1000,500,200,100,50') 
#check print                
        print_check(summa)
    elif getmoney % 50 != 0:
        print('Невозможно снять данную сумму. Банкомат выдает купюры: 5000,1000,500,200,100,50')
    else:
        print('Ошибка. Вставьте карту еще раз и повторите действие')
        
#if the user needs to take another action      
    print('Введите действие (положить\завершить): ')
    p = input()   
#if you chose to put money
    if p == 'положить':
        depositmoney = int(input('Введите необходимую сумму для внесения: '))
        if (depositmoney > 0) and (depositmoney % 50 == 0):
            while depositmoney >= 50:      
                if (depositmoney % 50 == 0):
                    print('Вставьте деньги')
                    summa = summa + depositmoney
                    break
                elif (depositmoney % 50 != 0):
                    print('Невозможно внести данную сумму. Банкомат вносит купюры: 5000,1000,500,200,100,50')
                else:
                    print('Операция не выполняется')
                    break
#check print
            print_check(summa)         
        elif depositmoney % 50 != 0:
            print('Невозможно внести данную сумму. Банкомат вносит купюры: 5000,1000,500,200,100,50')
        else:
            print('Ошибка. Вставьте карту еще раз и повторите действие')
    elif p == 'завершить':
        print('Операция завершена')
    else:
        print('Ошибка. Вставьте карту еще раз и повторите действие')
#if you chose to put money       
elif p == 'положить':
    depositmoney = int(input('Введите необходимую сумму для внесения: '))
    if (depositmoney > 0) and (depositmoney % 50 == 0):
        while depositmoney >= 50:      
            if (depositmoney % 50 == 0) :
                print('Вставьте деньги')
                summa = summa + depositmoney
                break
            elif (depositmoney % 50 != 0):
                print('Невозможно внести данную сумму. Банкомат вносит купюры: 5000,1000,500,200,100,50')
            else:
                print('Операция не выполняется')
                break
#check print
        print_check(summa)
    elif depositmoney %50 != 0:
        print('Невозможно внести данную сумму. Банкомат вносит купюры: 5000,1000,500,200,100,50')
    else:
        print('Ошибка. Вставьте карту еще раз и повторите действие')
    print('Введите действие (снять\завершить): ')
    p = input()
#if the user needs to take another action
    if p == 'снять':    
        getmoney = int(input('Введите необходимую сумму для снятия: '))
        if (getmoney > 0) and (getmoney % 50 == 0):
            while getmoney >= 50:
                if (getmoney % 50 == 0) and (getmoney <= summa) and (getmoney >= 50):
                    print('Заберите деньги')
                    summa = summa - getmoney 
                    break
                elif (getmoney > summa):
                    print('Недостаточно средств')
                    break
                else:
                    print('Невозможно снять данную сумму. Банкомат выдает купюры: 5000,1000,500,200,100,50')
                    break
#check print
            print_check(summa)
    elif p == 'завершить':
        print('Операция завершена')
    else:
        print('Ошибка. Вставьте карту еще раз и повторите действие')
else:
    print('Ошибка. Вставьте карту еще раз и повторите действие')
